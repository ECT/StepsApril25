#!/bin/bash
curl -O https://bkmgit.github.io/lc-shell/data/shell-lesson.zip
unzip shell-lesson.zip -d shell-lesson
cd shell-lesson
mkdir firstdir
mv 829-0.txt gulliver.txt
cp gulliver.txt gulliver-backup.txt
echo "Finally it is nice and sunny on" $(date)
touch a.txt b.txt c.txt d.txt
for filename in ?.txt
do
  echo "$filename"
  cp "$filename" backup_"$filename"
done
touch backup.sh
echo "#!/bin/bash" >> backup.sh
echo "# This script loops through .tsv files and makes" >> backup.sh
echo "# a backup" >> backup.sh
echo "for file in *.tsv" >> backup.sh
echo "do" >> backup.sh
echo '    echo "$file"' >> backup.sh
echo '    cp "$file" backupagain_"$file"' >> backup.sh
echo "done" >> backup.sh
bash backup.sh
head -n 3 2014-01_JA.tsv
wc *.tsv
wc -l *.tsv
wc -l *.tsv > lengths.txt
sort -n lengths.txt > sorted-lengths.txt
cat sorted-lengths.txt 
